var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var path = require("path");

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var import_path = function(relative_path){
    return path.join(__dirname, relative_path);
}

var routes = require(path.join(__dirname, "/routes/index"));
var users = require('./routes/users');

var DEBUG = true;

var moment = require('moment');

var http = require('http').Server(app);
var io = require('socket.io')(http);

var app = module.exports = express();

app["DEBUG"] = DEBUG;

var mysql = require("./database/MySQL")

var events = require('events');
var eventEmitter = new events.EventEmitter();

var session_model = require("./models/Session")(app,mysql.connection);
var parser = require("./lib/Parser");


var message_model = require("./models/Message")(app,mysql.connection);

var user_model = require("./models/User")(app,mysql.connection);

//var sessionStore = new SessionStore(options)

var redis   = require('redis');
rc = redis.createClient();

rc.on("connect", function() {
    rc.subscribe("MESSAGE_SENT");
    rc.subscribe("CHAT_MESSAGE_SENT");
    rc.subscribe("MESSAGE_UNREAD_COUNT");
    rc.subscribe("WHITEBOARD_MESSAGE");
    rc.subscribe("PN_CHANNEL"); //Push Notification Publish.
    rc.subscribe("CONVERSATION_TITLE_CHANGED"); //Push Notification Publish.
    rc.subscribe("CONVERSATION_BUDDY_ADDED");
    rc.subscribe("CONVERSATION_BUDDY_LEFT");
    rc.subscribe("CHAT_SETTINGS_UPDATED");
    rc.subscribe("USER_BLOCKED");
    rc.subscribe("USER_UNBLOCKED");
    rc.subscribe("NOTIFY_DRAWINGBOARD_ADDED");
    rc.subscribe("NOTIFY_TEXT_PAD_ADDED");
    rc.subscribe("NOTIFY_CODE_PAD_ADDED");
    rc.subscribe("NOTIFY_CODE_PAD_LAN_ADDED");
});

//console.log(rc)

rc.on("message", function (channel, message) {
    var json_data = JSON.parse(message);
    //console.log(json_data);
//    console.log(channel)

    if(channel == "CHAT_MESSAGE_SENT") {
        //console.log(json_data.receivers);
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data.content[receiver];
//            console.log(packet.remote_peers);
            //console.log(packet);
            console.log("Sending to: "+receiver);

            io.to(json_data.receivers[i]).emit("CHAT_MESSAGE_RECEIVED",packet);
        }
    }

    if(channel == "USER_BLOCKED") {
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var data = json_data.content[receiver];
            io.to(json_data.receivers[i]).emit("USER_BLOCKED",data);
        }
    }

    if(channel == "USER_UNBLOCKED") {
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var data = json_data.content[receiver];
            io.to(json_data.receivers[i]).emit("USER_UNBLOCKED",data);
        }
    }

    if(channel == "NOTIFY_DRAWINGBOARD_ADDED") {
        //console.log(json_data.receivers);
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data.data;
//            console.log(packet.remote_peers);
            //console.log(packet);
            console.log("Sending to: "+receiver);

            io.to(json_data.receivers[i]).emit("ON_DRAWINGBOARD_ADDED",packet);
        }
    }

    if(channel == "NOTIFY_TEXT_PAD_ADDED") {
        //console.log(json_data.receivers);
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data.data;
            console.log("Sending to: "+receiver);

            io.to(json_data.receivers[i]).emit("ON_TEXT_PAD_ADDED",packet);
        }
    }

    if(channel == "NOTIFY_CODE_PAD_ADDED") {
        //console.log(json_data.receivers);
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data.data;
            console.log("Sending to: "+receiver);

            io.to(json_data.receivers[i]).emit("ON_CODE_PAD_ADDED",packet);
        }
    }

    if(channel == "NOTIFY_CODE_PAD_LAN_ADDED") {
        //console.log(json_data.receivers);
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data.data;

            io.to(json_data.receivers[i]).emit("ON_CODE_PAD_LAN_CHANGED",packet);
        }
    }

    if(channel == "CONVERSATION_TITLE_CHANGED") {
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data;

            io.to(json_data.receivers[i]).emit("CONVERSATION_TITLE_CHANGED",packet);
        }
    }

    if(channel == "CONVERSATION_BUDDY_ADDED") {
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data;

            io.to(json_data.receivers[i]).emit("CONVERSATION_BUDDY_ADDED",packet);
        }
    }

    if(channel == "CONVERSATION_BUDDY_LEFT") {
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data;

            io.to(json_data.receivers[i]).emit("CONVERSATION_BUDDY_LEFT",packet);
        }
    }

    if(channel == "CHAT_SETTINGS_UPDATED") {
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data.data;

            io.to(json_data.receivers[i]).emit("CHAT_SETTINGS_UPDATED",packet);
        }
    }

    if(channel == "MESSAGE_UNREAD_COUNT") {
        if(json_data.status == "SUCCESS"){
            var packet = {
                unread_message_count: json_data.unread_message_count
            }
            io.to(json_data.receiver_id).emit("MESSAGE_UNREAD_COUNT_RECEIVED",packet);
        }
    }

    if(channel == "WHITEBOARD_MESSAGE"){
        var receivers = json_data.data.receivers;
        console.log(receivers);
        for(var j = 0 ; j < receivers.length ; j++ ){
            io.to(receivers[j]).emit("WHITEBOARD_MESSAGE",JSON.stringify(json_data.data.message_entry));
        }
    }

    if(json_data.status == "SUCCESS"){
        if(channel == "MESSAGE_SENT"){
            var data = json_data.data;
            console.log(data.sender_id);
            var sender_id = json_data.sender_id;
            var receivers = data.receivers;
            var sender = { id: data.sender_id, name: data.sender_name }
//            console.log(sender);
            receivers.push(sender);
            for(var i = 0 ; i < receivers.length; i++) {
//                console.log("Message Sending to: " + receivers[i]);
                var data_stream = {
                    recipient: receivers[i].id,
                    data: data,
                    sender_id: sender_id,
                    sender_name: receivers[i].name
                }
                console.log("message sending to: "+receivers[i].id);
                io.to(receivers[i]).emit("MESSAGE_RECEIVED",JSON.stringify(data_stream));
            }
        }
        else if(channel == "PN_CHANNEL") {
            console.log("Publishing...")
            var send_to = json_data.target_user;
            console.log(send_to);
            io.to(send_to).emit("PN_RECEIVE",JSON.stringify(json_data));
        }
    }
});

var connected_clients = {};

//var subscribe_presence = require('child_process').fork('./subscribe_presence.js');

var on_socket_connection = function(socket)
{

    var user_session_data;

    if(DEBUG)
    {
        console.log("A client connected.");
    }

    //subscribe_presence.send("socket","asdasd");
    
    var client_session = parser.parse_cookies(socket.request.headers.cookie);
    var session_id = client_session.sessionid;
    console.log(session_id)

    session_model.read_session_DB(session_id,function(session_data_json)
    {
        if(DEBUG)
        {
            console.log("Inside client connection success callback.");
            console.log(session_data_json);
        }

        user_session_data = session_data_json;

        socket.join(session_data_json.user_id);

        //Save online status.
        user_model.update_online_status({ id: session_data_json.user_id, status: 1 });

    },
    function(err_obj)
    {

    });


    socket.on("update_tz", function(data){
        
        
    });

    socket.on("online_status", function(data)
    {
        if(data.status == true && data.uid != -1 && !connected_clients.hasOwnProperty(data.uid))
        {
            connected_clients[data.uid] = data.status;
            eventEmitter.emit("online_status_changed");
        }
        else if(data.status == false && connected_clients.hasOwnProperty(data.uid))
        {
            delete connected_clients[data.uid];
            eventEmitter.emit("online_status_changed");
        }
        if(DEBUG)
        {
            console.log("Online users: ");
            console.log(connected_clients);
        }
    });

    //subscribe_presence.send("socket",socket);

    // Start listening for mouse move events
//    socket.on('mousemove', function (data) {
//
//        socket.broadcast.emit('whiteboard_data', data);
//    });

    socket.on("send_stream", function(stream_object) {
        //console.log("Send stream received...");
        //stream_object will be in the follwoing format.
        /*
        * stream_object = {
        *     event_name: on_mouse_down,
        *     lesson_id: 1,
        *     whiteboard_id: 1,
        *     target_users: [1,2],
        *     drawing_info: {
        *         start_point: Point,
        *         points: [],
        *         tool
        *     }
        *     canvas_id: 1,
        *     drawing_state: start/end
        * }
        * */
        //console.log(stream_object);
        var target_users = stream_object.target_users;
        for(var i = 0 ; i < target_users.length ; i++) {
            io.to(parseInt(target_users[i])).emit("receive_stream",stream_object);
        }

    });

    socket.on("notify_board_added", function(stream_object) {
        console.log(stream_object);
        var target_users = stream_object.target_users;
        for(var i = 0 ; i < target_users.length ; i++) {
            io.to(parseInt(target_users[i])).emit("on_board_added",stream_object);
        }

    });

    socket.on('sendchatmessage', function (data) {

        if(DEBUG)
        {
            console.log("Message received: ");
            console.log(data);
        }

        //Get all publisher and subscribers and broadcast the message to them.
        var uids = [];
        var user_messges = [];
        var pub_id = data.publisher.uid;
        var subscribers = data.subscribers.uids;
        uids.push(pub_id);
        for(var i = 0 ; i < subscribers.length ; i++){
            uids.push(subscribers[i]);
            user_messges.push({
                "sender_id": pub_id,
                "receiver_id": subscribers[i]
            });
        }

        if(DEBUG){
            console.log(user_messges);
        }

        message_model.save_chat(
            {
                "msg": data.message,
                "read": 0,
                "current_utc_timestamp": moment.utc().unix(),
                "chat_type": data.chat_type,
                "msg_users": user_messges
            },
            function(){
                if(DEBUG){
                    console.log("Message Saved Successfully.");
                }
                //Now prepare the message packet and broadcast to all.
                var packet = data;

                user_model.get_user(uids,function(data){
                    console.log("Inside get_user success callback.");
                    console.log(data);
                    console.log(pub_id);

                    for(var j = 0 ; j < uids.length ; j++){
                        packet["user"] = data[pub_id];
                        io.to(parseInt(uids[j])).emit("onchatmessage",packet);
                    }

                },function(error){
                    console.log("Inside get_user error callback.");
                    console.log(error);
                });

//                packet["name"] = "Another User";
//
//                for(var j = 0 ; j < uids.length ; j++){
//                    io.to(parseInt(uids[j])).emit("onchatmessage",packet);
//                }
            },
            function(){
                if(DEBUG){
                    console.log("Message Saving Failed.");
                }
            });

        // var publisher_id = parseInt(data.local_peer.uid,10);
        // var subsciber_id = parseInt(data.remote_peer.uid,10);
        // var msg = data.msg;
        
        // insert_chat_message_session_DB(
        //     {
        //         "publisher_id":publisher_id,
        //         "subsciber_id":subsciber_id,
        //         "msg":msg
        //     },
        //     function()
        //     {
        //         console.log("Inside success callback.");
        //         io.to(subsciber_id).emit("onchatmessage",data);
        //     },
        //     function(err_obj)
        //     {

        //     });
        
    });

    socket.on('pub_notif',function(data)
    {
        var publish_to = data.subscriber.uid;
        if(DEBUG)
        {
            console.log(data);
            console.log("Publishing to: "+publish_to);    
        }
        
        io.to(parseInt(publish_to,10)).emit("on_sub_notif",data);
    });

    socket.on('_disconnect',function(data)
    {
        console.log("Other tabs open!");
        user_model.update_online_status({ id: data.user_id, status: 1 });
    });

    socket.on('ask_whiteboard_presence',function(data)
    {
        var other_participants = data.other_participants;
        var receivers = [];
        for(var i = 0 ; i < other_participants.length; i++) {
            if(other_participants[i] != data.sender_id) {
                io.to(other_participants[i]).emit("whiteboard_user_presence_ask_received",{ user_id: data.sender_id });
            }
        }
    });

    socket.on('notify_whiteboard_presence',function(data)
    {
        var receiver = data.receiver;
        io.to(receiver).emit("whiteboard_user_presence_received",{ user_id: data.sender_id });
    });

    socket.on('notify_whiteboard_exit',function(data)
    {
        io.to(data.sender_id).emit("any_other_whiteboard_tab_open",data);

        var receivers = [];
        for(var i = 0 ; i < other_participants.length; i++) {
            if(other_participants[i] != data.sender_id) {
                io.to(other_participants[i]).emit("notify_whiteboard_exit_received",{ user_id: data.sender_id });
            }
        }

    });

    socket.on('open_whiteboard_found',function(data)
    {
        var other_participants = data.other_participants;
        var receivers = [];
        for(var i = 0 ; i < other_participants.length; i++) {
            if(other_participants[i] != data.sender_id) {
                var receiver = other_participants[i];
                io.to(receiver).emit("whiteboard_user_presence_received",{ user_id: data.sender_id });
            }
        }
    });

    socket.on('disconnect', function()
    {
        if(DEBUG)
        {
            console.log("Client disconnected.");
        }

        var client_session = parser.parse_cookies(socket.request.headers.cookie);
        var session_id = client_session.sessionid;
        console.log(session_id)

        session_model.read_session_DB(session_id,function(session_data_json)
        {
            if(DEBUG)
            {
                console.log("Checking if there any other tab oppened.");
                console.log(session_data_json);
            }

            io.to(session_data_json.user_id).emit("still_alive",{});
            user_model.update_online_status({ id: session_data_json.user_id, status: 0 });

        },
        function(err_obj)
        {

        });

    });

    eventEmitter.on("online_status_changed",function()
    {
        if(DEBUG)
        {
            console.log("Online status changes.");
            console.log("Now online users: ");
            console.log(connected_clients);
            console.log("Now subscribe online users to all connected clients.");
        }

        for(var uid in connected_clients)
        {
            io.to(parseInt(uid,10)).emit("on_subscribe_online_users",connected_clients);
        }

    });

}

io.on("connection",on_socket_connection);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

io.set('authorization', function (handshakeData, accept) {

  if (handshakeData.headers.cookie) {

    //handshakeData.cookie = cookieParser.parse(handshakeData.headers.cookie);

    if(DEBUG){
        console.log(handshakeData.headers.cookie.sessionid);
    }

    }
  accept(null, true);
});
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', function(req,res,next)
    {
        
    });
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

module.exports = app;
