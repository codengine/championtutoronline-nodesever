var app = require("../app");
var mysql = require('mysql');

var connection = mysql.createConnection({
		  host     : 'localhost',
		  user     : 'root',
		  password : 'root'
		});

if(app.DEBUG){
	console.log(connection);
}

connection.query('USE champ_wb2');

var exports = module.exports = {
	connection: connection
};
